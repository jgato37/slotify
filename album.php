<!-- The Header  -->
<?php include("includes/header.php");
	if(isset($_GET['id'])){
		$albumId = $_GET['id'];
	}
	else{
		header("Location: index.php");
	}

	//$artistId = $album['artist']; //We dont really need this line again, we created the
	//variable so that the single quotes in there will not interferee with our  sql
	//statements in the sqli function

	$album = new Album($con, $albumId);
	$artist = $album->getArtist();
?>

	<div class="entityInfo">

		<div class="leftSection">
			<img src="<?php echo $album->getArtworkPath(); ?>">
		</div>

		<div class="rightSection">
			<h2><?php echo $album->getTitle(); ?></h2>
			<p>By <?php echo $artist->getName(); ?></p>
			<p><?php echo $album->getNumberOfSongs(); ?> Song(s)</p>
		</div>

	</div>

	<div class="tracklistContainer">
		<ul class="tracklist">
			<?php
				$songIdArray = $album->getSongIds();

				$i = 1;
				foreach($songIdArray as $songId) {
					$albumSong = new Song($con, $songId);
					$albumArtist = $albumSong->getArtist();

					echo "<li class='tracklistRow'>
							<div class='trackCount'>
								<img class='play' src='assets/images/icons/play-white.png '>
								<span class='trackNumber'>$i</span>
							</div>

							<div class='trackInfo'>
								<span class='trackName'>" . $albumSong->getTitle() . "</span>
								<span class='artistName'>" . $albumArtist->getName() . "</span>
							</div>

							<div class='trackOptions'>
								<img class='optionsButton' src='assets/images/icons/more.png'>
							</div>

							<div class='trackDuration'>
								<span class='duration'>" . $albumSong->getDuration() . "</span>
							</div>

						</li>";
					$i = $i + 1;
				}
			?>
		</ul>
	</div>


<!-- The Footer -->
<?php include("includes/footer.php"); ?>